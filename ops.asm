;=============================================
; Program Description: Boolean Calculator
; Author: Christopher Rains
; Date Created: 24 June 2011
; Last Modification Date: 22 July 2013
;=============================================

INCLUDE Irvine32.inc

.data

menuTxt1 BYTE "1. x AND y", 0
menuTxt2 BYTE "2. x OR y", 0
menuTxt3 BYTE "3. NOT X", 0
menuTxt4 BYTE "4. x XOR y", 0
menuTxt5 BYTE "5. Exit Program",0
menuTxt6 BYTE "Please enter your number choice: ", 0

functionTxt BYTE "Please enter two hexidecimal numbers separated by returns:", 0
functionTxt2 BYTE "Please enter one hexidecimal number:",0

resultPrompt BYTE "The result of your operation is: ", 0

waitPrompt BYTE "Press Return to Continue", 0

answer DWORD 0

.code
main PROC

	menu:
		call Clrscr
		mov edx, OFFSET menuTxt1		;Prints the menu text to the screen
		call WriteString
		call Crlf
		mov edx, OFFSET menuTxt2
		call WriteString
		call Crlf
		mov edx, OFFSET menuTxt3
		call WriteString
		call Crlf
		mov edx, OFFSET menuTxt4
		call WriteString
		call Crlf
		mov edx, OFFSET menuTxt5
		call WriteString
		call Crlf

		call crlf				;Prints the choice prompt
		mov edx, OFFSET menuTxt6	
		call WriteString
		call Crlf

		call ReadInt				;Reads in one integer choice from the
							;user.
		cmp eax, 1				;Compare answer choice to determine
			je Sit1				;proper situation to launch.
		cmp eax, 2
			je Sit2
		cmp eax, 3
			je Sit3
		cmp eax, 4
			je Sit4
		cmp eax, 5
			je END_PROGRAM		
			jne MENU			;If no choices match, incorrect input
							;entered. Return to menu.
		
		Sit1:					;Situation receives two hex numbers
			call Clrscr			;and then launches AND_op.
			mov edx, OFFSET functionTxt
			call WriteString		;Prompt user for hex numbers
			call Crlf
			call ReadHex
			mov ebx, eax
			call ReadHex
			push eax			;Push hex values to stack
			push ebx
			call AND_op			;Call procedure			
			pop ebx				;Pop values back from stack
			pop eax
			jmp menu			;Procedure returns to main.
		
		Sit2:					;Situation recieves two hex numbers
			call Clrscr			;and then launches OR_op.
			mov edx, OFFSET functionTxt
			call WriteString		;Prompt user for hex numbers
			call Crlf
			call ReadHex
			mov ebx, eax
			call ReadHex
			push eax			;Push hex values to stack
			push ebx
			call OR_op			;Call procedure
			pop ebx				;Pop values back from stack
			pop eax
			jmp menu			;Procedure returns to main.
		
		Sit3:					;Situation receives one hex number
			call Clrscr			;and then launches NOT_op.
			mov edx, OFFSET functionTxt	
			call WriteString		;Prompt user for hex number
			call Crlf
			call ReadHex
			push eax			;Push hex value to stack
			call NOT_op			;Call procedure
			pop eax				;Pop value back from Stack
			jmp menu			;Procedure returns to main.
		
		Sit4:					;Situation receives two hex numbers
			call Clrscr			;and then launches NOT_op.
			mov edx, OFFSET functionTxt	
			call WriteString		;Prompt user for hex numbers
			call Crlf
			call ReadHex			
			mov ebx, eax
			call ReadHex
			push eax			;Push hex values to stack
			push ebx
			call XOR_op			;Call procedure
			pop ebx				;Pop values back from stack.
			pop eax
			jmp menu			;Procedure returns to main.

		END_PROGRAM:				;If answer choice '5' is entered, jump
							;here and end the program.

	exit
main ENDP

;=====================================================
;Function Name: AND_op
;Purpose: To AND two numbers together
;Input: 2 Numbers (passed by user)
;Output: User Input ANDed together
;=====================================================

AND_op PROC

	push ebp					;Push base pointer to stack
	mov ebp, esp					;Set ebp to static value of esp
	mov eax, [ebp+8]				;Pull values off stack into registers
	mov ebx, [ebp+12]
	and eax, ebx					;AND the two hex numbers
	shr eax, 8
	call Crlf
	mov edx, OFFSET resultPrompt			
	call WriteString				;Print the result to the screen.
	call WriteHex
	call Crlf
	
	call WaitMsg					;Prompt wait message until the user 
							;presses the return key.
	pop ebp						;Pop ebp off stack
	ret 8						;Clean stack

AND_op ENDP

;=====================================================
;Function Name: OR_op
;Purpose: To OR two numbers together
;Input: 2 Numbers (passed by user)
;Output: User Input ORed together
;=====================================================

OR_op PROC

	push ebp					;Push base pointer to stack.
	mov ebp, esp					;Set ebp to static value of esp.
	mov eax, [ebp+8]				;Pull values off stack into registers
	mov ebx, [ebp+12]
	or eax, ebx					;OR the two hex numbers
	shr eax, 8
	call Crlf
	mov edx, OFFSET resultPrompt
	call WriteString				;Print the result to the screen.
	call WriteHex
	call Crlf

	call WaitMsg					;Prompt wait message until the user 
							;presses the return key.
	pop ebp						;Pop ebp off stack
	ret 8						;Clean stack

OR_op ENDP

;=====================================================
;Function Name: NOT_op
;Purpose: To NOT a number passed by the user
;Input: 1 Number (passed by user)
;Output: User Input NOTed
;=====================================================

NOT_op PROC

	push ebp					;Push base pointer to stack
	mov ebp, esp					;Set ebp to static value of esp
	mov eax, [ebp+8]				;Pull value off stack into register
	not eax						;NOT the hex number
	sar eax, 8
	call Crlf
	mov edx, OFFSET resultPrompt
	call WriteString				;Print the result to the screen.
	call WriteHex
	call Crlf	

	call WaitMsg					;Prompt wait message until the user												
							;presses the return key.
						
	pop ebp						;Pop ebp off stack
	ret 8						;Clean stack

NOT_op ENDP

;=====================================================
;Function Name: XOR_op
;Purpose: To EXCLUSIVE OR two numbers together
;Input: 2 Numbers (passed by user)
;Output: User Input XORed together
;=====================================================

XOR_op PROC
	
	push ebp					;Push base pointer to stack
	mov ebp, esp					;Set ebp to static value of esp
	mov eax, [ebp+8]				;Pull value off stack into register
	mov ebx, [ebp+12]			
	xor eax, ebx					;XOR the two hex numbers
	shr eax, 8
	call Crlf
	mov edx, OFFSET resultPrompt
	call WriteString				;Print the result to the screen.
	call WriteHex
	call Crlf

	call WaitMsg					;Prompt wait message until the user
							;presses the return key.					
	pop ebp						;Pop ebp off stack
	ret 8						;Clean stack

XOR_op ENDP

END main						;End Program.